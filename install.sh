#!/bin/bash

SCRIPT_DIR="$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"

mkdir -p $HOME/.local/share/icons &>/dev/null

cp -f $SCRIPT_DIR/icons/HP-Firefox-icon-128.png $HOME/.local/share/icons/HP-Firefox-icon-128.png

cp -f $SCRIPT_DIR/resources/run.sh $HOME/.local/bin/firefox_podman_run.sh

chmod 700 $HOME/.local/bin/firefox_podman_run.sh

cat << EOF > $HOME/.local/share/applications/firefox_podman.desktop
[Desktop Entry]
Type=Application
Encoding=UTF-8
Name=Firefox Podman
Comment=Web Browser
Icon=$HOME/.local/share/icons/HP-Firefox-icon-128.png
Exec=$HOME/.local/bin/firefox_podman_run.sh
Terminal=false
Categories=Tags;Describing;Application
EOF

chmod 600 $HOME/.local/share/icons/HP-Firefox-icon-128.png $HOME/.local/share/applications/firefox_podman.desktop
